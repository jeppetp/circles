var width = window.innerWidth
var height = window.innerHeight

// euclidean distance
let dist = ((p1, p2) => sqrt((p1[0] - p2[0])**2 + (p1[1] - p2[1])**2))

// angle between y-axis and the vector that spans [p1, p2]
let angle = (p1, p2) => Math.atan2(p2[1] - p1[1], p2[0] - p1[0])

// move a point or a triangle by v
let move_thing = (xys, v) => xys.map((e, i) => e + (i%2==0 ? v[0] : v[1]))

// rotate a point or a triangle around point c by theta
function rotate_thing(yys, c, th) {
  let at_origin = move_thing(yys, [-c[0], -c[1]])
  let rotated = at_origin.map((e, i, l) =>
    i%2 == 0
    ? e * cos(th) - l[i+1] * sin(th)  // ys
    : e * cos(th) + l[i-1] * sin(th)  // ys
  )
  return move_thing(rotated, c)
}

// ...I miss python
let range = ((n) => [...Array(n).keys()])

function windowResized() {
  width = windowWidth
  height = windowHeight
  resizeCanvas(width, height)
}

function setup() {
  createCanvas(windowWidth, windowHeight)
  colorMode(HSB, 255)
  frameRate(30)
}


// yes this code is poop but the circle is pretty so ⚫
const swiggleSpeed = 0.05
function swiggle(p, c, t) {
  let segLen = 10
  let wiggleAmount = 1.8
  let thetaPC = angle(p, c)

  let wiggleA = wiggleAmount * (noise(p[0], p[1], p[0] + t * swiggleSpeed) - 0.5)
  let wiggleB = wiggleAmount * (noise(p[0], p[1], p[1] + t * swiggleSpeed) - 0.5)

  let a = [p[0] + (segLen * cos(wiggleA + thetaPC)), p[1] + (segLen * sin(wiggleA + thetaPC))]
  let b = [p[0] - (segLen * cos(wiggleB + thetaPC)), p[1] - (segLen * sin(wiggleB + thetaPC))]

  line(...p, ...a)
  line(...p, ...b)
}

var t = 0
const appearSpeed = 0.0005
let circles = height*(1/12)

function draw() {
  background(180, 50, 20)
  strokeWeight(2)
  let center = [width/2, height/2]

  noFill();
  for (i=0; i<circles; i++){
    stroke((255/circles)*i, 150, 180)
    arc(center[0], center[1], ((i+1) / circles)*height*(2/3), ((i+1) / circles)*height*(2/3),(mouseY - height/2) / (200 * ((i+1) / circles)), (mouseX - width/2) / (200 * ((i+1) / circles)))
  }


  // for (j=0; j < 5; j++){
  //   for (i=0; i<strings; i++) {
  //     stroke(180, 80, 80);
  //     array = [100, 400, 200, 400 + 200*sin(t + i*(2*PI)/strings), 300, 400 + 200*cos(t+ i*(2*PI)/strings), 400, 400]
  //     zeroarray = move_thing(array, [-2200, -200])
  //     rotated_nonmoved_array = rotate_thing(zeroarray, [0,0], 0.3*t + j * 2*PI/5)
  //     strokeWeight(6+3*sin(t + i*(2.0*PI)/strings + 3*PI/4))
  //     rotated_array = move_thing(rotated_nonmoved_array, [width/2,height/2])
  //     bezier(...rotated_array)
  //   }
  // }
  // background(34, 15, 2200)
  // let centerP = [width / 2, height / 2]
  // swiggle([300, 300], centerP, t)
  //
  // strokeWeight(1.0)
  // stroke(158, 15, 80)
  // let maxR = min([width / 2, height / 2])
  // for (var R = 0; R < maxR; R += 10) {
  //   let vC = R
  //   let threshold = (Math.cos(vC * 0.03 + (t * 0.4)) * 0.5) + 0.5
  //
  //   for (var th = 0; th < 2 * PI; th += 0.01) {
  //     let r = (R + noise(t + th) * 5)
  //     let theta = th + noise(t + th)
  //     let here = [centerP[0] + (cos(theta) * r),
  //                 centerP[1] + (sin(theta) * r)]
  //
  //     // let vC = dist(here, centerP)
  //     let appear = noise(here[0], here[1], t * appearSpeed)
  //     if (appear < threshold) {
  //       swiggle(here, centerP, t)
  //     }
  //   }
  // }

  t += 0.01
}
